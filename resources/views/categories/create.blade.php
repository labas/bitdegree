<form action="{{ url('categories/create') }}" method="POST">
    <div class="row">
        <div class="col-md-12" style="text-align: left; margin-bottom: 20px">
            <label for="title">Title</label>
            <input type="text" id="title" name="title" class="form-control" placeholder="Title" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align: left; margin-bottom: 20px">
            <label for="parent">Parent Category</label>
            <select class="form-control" id="parent" name="parent_id">
                <option value="0" selected>(none)</option>
                @foreach($categories as $category)
                    @include('categories.partials.select')
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="text-align: left; margin-bottom: 20px">
            <button type="submit" class="btn btn-success btn-block">Create</button>
        </div>
    </div>
</form>