<li>{{ $category->title }}</li>
@if (count($category->children) > 0)
	<ul>
		@foreach($category->children as $category)
			@include('categories.partials.list', $category)
		@endforeach
	</ul>
@endif