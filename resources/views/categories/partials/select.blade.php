<option value="{{ $category->id }}">{{ str_repeat('-', $loop->depth) }} {{ $category->title }}</option>
@if (count($category->children) > 0)
    @foreach($category->children as $category)
        @include('categories.partials.select', $category)
    @endforeach
@endif
	