@extends('master')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success">
            Category successfully created
        </div>
    @endif
    @include('categories.create')

    <h4>Categories List</h4>
    <div class="row">
        <div class="col-md-12" style="text-align: left">
            <ul>
                @foreach($categories as $category)
                    @include('categories.partials.list')
                @endforeach
            </ul>
        </div>
    </div>
@stop