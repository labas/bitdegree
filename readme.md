# BitDegree Test Task
### Installation

```sh
$ git clone https://gitlab.com/labas/bitdegree.git
$ composer install
$ cp .env.example .env
$ php artisan key:generate
```

Define database credentials in `.env` file

```env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=insert_database_name
DB_USERNAME=insert_database_username
DB_PASSWORD=insert_database_user_password
```

Run the migration

```sh
$ php artisan migrate
```

Open up the application :)

# Testing

```sh
$ cd application_dir
$ vendor/bin/phpunit
```
