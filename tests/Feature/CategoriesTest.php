<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoriesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_can_create_a_category()
    {
        $category = Category::create(['title' => 'A', 'parent_id' => 0]);

        $this->assertDatabaseHas('categories', $category->toArray());
    }

    /** @test */
    public function it_can_create_categories_with_childrens()
    {
        $cat1 = Category::create(['title' => 'A', 'parent_id' => 0]);
        $cat2 = Category::create(['title' => 'AA', 'parent_id' => $cat1->id]);
        $cat3 = Category::create(['title' => 'AAA', 'parent_id' => $cat2->id]);

        $this->assertEquals(
            $cat1->children->first()->toArray(),
            $cat2->toArray()
        );

        $this->assertEquals(
            $cat2->children->first()->toArray(),
            $cat3->toArray()
        );
    }
}
