<?php

namespace App\Http\Controllers;

use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::with('children')
                            ->where('parent_id', 0)
                            ->get();

        return view('categories.index', compact('categories'));
    }

    public function store()
    {
        Category::create(request()->all());

        return redirect()->back()->with('success', true);
    }
}
